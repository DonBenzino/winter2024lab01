import java.util.Scanner;
public class HangMan{
	public static void main(String[] args){
		//Establish the Scanner variable and save the word
		Scanner reader = new Scanner(System.in);
		System.out.println("What is the word?");
		String Word = reader.nextLine();
		
		//Clears the screen (so that you can't cheat)
		System.out.print("\033[H\033[2J");
		runGame(Word);
	}
	public static void runGame(String Word){
		//Establish the Scanner variable once again
		Scanner reader = new Scanner(System.in);
		
		//Sets the lives
		int Lives = 6;
		
		//Creates char variable for current guess (E used because '' causes errors)
		char Guess = 'E';
		
		//Copies Word to be shown at the end
		String Failsafe = Word;
		
		//Creates String variable for past guesses (wrong or right)		
		String Prev = "";
		
		//Creates Array equal to the size of the word for the found letters (the loops makes all unfound ones '_')
		char[] Found = new char[Word.length()];
		for(int i = 0; i<Found.length; i++){
			Found[i] = '_';
		}
		
		//Sets the starting GameState
		boolean GameState = false;
		
		//*****MAIN LOOP*****
		while (Lives != 0){
			
			//This if statement checks if all letters have been found (line 57 for my method)
			if (Word.replace("0","").equals("")){
				GameState = true;
				break;
			}
			
			//Asks for next guess
			System.out.println("What is your guess?");
			Guess = reader.next().charAt(0);
			
			//Calls the isLetterInWord Function to check if... the letter is in word
			if (isLetterInWord(Word, Guess) == true){
				
				// if previous statementis true, runs loop to remove the letter from the correct guesses string (Word) 
				//and adds it to the array of discovered letters in the correct position (Found[])
				for(int i = 0; i<Found.length; i++){
					if (Character.toUpperCase(Word.charAt(i)) == Character.toUpperCase(Guess)){
						Found[i] = Word.charAt(i);
						Word = Word.substring(0,i) +  "0" + Word.substring(i+1);
					}
				}
				
				//add guessed letter to the pool of previously guessed letters, tells the users it was correct and shows them its positions in the word
				Prev = Prev + Guess;
				System.out.println("That letter is in the word");
				PrintWork(Found);

			// if it's not in the Word variable, this statement will check if it's in the pool of previously guessed letters
			}else if (isLetterInWord(Prev, Guess) == true){
				
				//if the previous statement is true, notifies the users and lets them try again
				System.out.println("That letter was already guessed, try again");
			}else{
				
				//add guessed letter to the pool of previously guessed letters, tells the users it was wrong, removes a life and tells them about it
				Prev = Prev + Guess;
				System.out.println("That letter is not in the word");
				Lives--;
				System.out.println("Lives left " + Lives);
			}
		}
		
		//if the while loop is broken, checks the games state for if the user won or didn't and tells them the correct answer
		if (GameState){
			System.out.println("You Won");
		}else{
			System.out.println("You lose, the word was " + Failsafe);
		}
	}
	
	//Function will check if the letter guessed is in the word
	public static boolean isLetterInWord(String Word, char Guess){
		//Loop will compare each letter in the word with the guessed letter (both in upper case to avoid errors)
		for (int i = 0; i<Word.length();i++){
			if(Character.toUpperCase(Word.charAt(i)) == Character.toUpperCase(Guess)){
				return true;
			}
		}
		return false;
	}
	
	//Function prints the discovered letters in there correct position
	public static void PrintWork(char[] Found){
		for(int i = 0; i<Found.length; i++){
			System.out.print(Found[i]);
		}
		//this acts as an enter key press
		System.out.println("");
	}
}